package org.mavegitjenkins.com.org.mavegitjenkins.com;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class helloworldtestng {
	
	WebDriver driver;
	
	@BeforeSuite
	public void loadall()
	{
		System.setProperty("webdriver.chrome.driver", "D:\\Java Documents\\chromedriver_win32\\updatedwnew\\chromedriver.exe");
	}
	@Parameters({"a","b"})
	@Test(priority=2)
	public void firstclass(int c,int d)
	{
		System.out.println("hello world using testng ");
		System.out.println("add:"+ (c+d));

	}
	
	@Test(priority=1)
	public void secondwish()
	{
		System.out.println("second wish");
		

	}
	@Test(priority=3, enabled=false)
	public void seleniumfirst()
	{
		
		
		driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.get("https://www.google.com");
		
	System.out.println(driver.getTitle()+"");
	

	}
	
	@AfterSuite
	public void teardown()
	{
		///driver.quit();
	}

}
